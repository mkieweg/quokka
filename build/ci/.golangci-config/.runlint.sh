golangci-lint run\
  --config .ci/.golangci-master.yml\
  --out-format code-climate |\
  jq -r '.[] | "\(.location.path):\(.location.lines.begin) \(.description)"'
